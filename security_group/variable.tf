variable "ingress_ports" {
  type    = "list"
  default = ["8080"]
}

variable "ingress_cidr_blocks" {
  type    = "list"
  default = ["0.0.0.0/0"]
}

variable "private_ingress_cidr_blocks" {
  type    = "list"
  default = ["0.0.0.0/0"]
}

variable "egress_cidr_blocks" {
  type    = "list"
  default = ["0.0.0.0/0"]
}

variable "sg_name" {
  default = "epam_project"
}
