provider "aws" {
  region  = var.region
  profile = var.profile
}

module "db" {
  source = "./db"

  database_name = "kate-project"
}
