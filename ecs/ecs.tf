resource "aws_ecs_cluster" "ecs_cluster" {
  name = var.cluster_name
}

resource "aws_launch_configuration" "launch_conf" {
  name                 = var.launch_conf_name
  image_id             = "${data.aws_ami.ecs_optimized.id}"
  iam_instance_profile = "${aws_iam_instance_profile.ecs_agent.name}"
  security_groups      = ["${aws_security_group.container_instance.id}"]
  user_data            = "${data.template_file.user_data.rendered}"

  instance_type = var.instance_type
}

data "aws_ami" "ecs_optimized" {
  owners = ["self", "amazon", "aws-marketplace"]
  filter {
    name   = "name"
    values = ["amzn-ami-2017.09.l-amazon-ecs-optimized"]
  }
}

data "template_file" "user_data" {
  template = file("${path.module}/scripts/user_data.yaml")

  vars = {
    cluster_name = "${aws_ecs_cluster.ecs_cluster.name}"
  }
}

resource "aws_security_group" "container_instance" {
  name = "container_instance-tf"
  dynamic "ingress" {
    for_each = ["8080", "22", "80", "443"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_autoscaling_group" "autoscaling_group" {
  name                 = var.autoscaling_group_name
  vpc_zone_identifier  = ["${var.subnet_ids}"]
  launch_configuration = "${aws_launch_configuration.launch_conf.name}"

  desired_capacity = 1
  min_size         = 1
  max_size         = 1
}
