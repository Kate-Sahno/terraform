variable "cluster_name" {
  default = "test"
}

variable "launch_conf_name" {
  default = "ecs_launch_conf"
}

variable "autoscaling_group_name" {
  default = "ecs_autoscaling_group"
}

variable "key_name" {
  default = "aws-sa-east-1"
}
variable "instance_type" {
  default = "t2.micro"
}

variable "subnet_ids" {
  default = "subnet-04962b62"
}
