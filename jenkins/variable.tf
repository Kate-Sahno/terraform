variable "access_key" {
  default     = ""
  description = "Kate user in main AWS"
}

variable "secret_key" {
  default     = ""
  description = "Kate user in main AWS"
}

variable "region" {
  default     = "sa-east-1"
  description = "Default Amazon region"
}
