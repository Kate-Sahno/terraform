provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

module "ec2" {
  source = "../ec2/"

  count_instances = 1
  key_name        = "aws-key-name"
  tags            = "jenkins-tf"
  ingress_ports   = ["8080", "12345"]
}
