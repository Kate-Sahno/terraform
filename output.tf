# DataBase output
output "db_endpoint" {
  value = "${module.db.endpoint}"
}

output "db_port" {
  value = "${module.db.port}"
}

output "db_name" {
  value = "${module.db.database_name}"
}
