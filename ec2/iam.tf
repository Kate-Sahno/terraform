resource "aws_iam_role" "ec2_adminRole_tf" {
  name               = "ec2_adminRole_tf"
  assume_role_policy = "${data.aws_iam_policy_document.assume_policy.json}"
}

resource "aws_iam_role_policy_attachment" "ec2_adminPolicy_tf" {
  role       = "${aws_iam_role.ec2_adminRole_tf.id}"
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_instance_profile" "ec2_profile_tf" {
  name = "ec2_profile_tf"
  role = "${aws_iam_role.ec2_adminRole_tf.name}"
}

data "aws_iam_policy_document" "assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}
