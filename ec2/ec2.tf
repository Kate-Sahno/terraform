resource "aws_instance" "masterUbuntu18" {
  count                  = var.count_instances
  ami                    = var.ami
  instance_type          = var.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.sg.id]
  iam_instance_profile   = "${aws_iam_instance_profile.ec2_profile_tf.name}"
  user_data              = file("${path.module}${var.user_data}")
  tags = {
    Name = var.tags
  }
}

resource "aws_security_group" "sg" {
  name = var.aws_security_group_name
  dynamic "ingress" {
    for_each = var.ingress_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = var.ingress_cidr_blocks
    }
  }
  # SSH conection
  ingress {
    from_port   = var.ssh_from_port
    to_port     = var.ssh_to_port
    protocol    = "tcp"
    cidr_blocks = var.ssh_ingress_cidr_blocks
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.egress_cidr_blocks
  }
}
