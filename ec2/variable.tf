variable "count_instances" {
  default = 1
}
variable "ami" {
  default     = "ami-05494b93950efa2fd"
  description = "Ubuntu server 18.04"
}

variable "instance_type" {
  default     = "t2.micro"
  description = "Free instance_type"
}

variable "key_name" {
  default     = "aws-sa-east-1"
  description = "Default Amazon aws_key_pair"
}

variable "tags" {
  default = "ec2_tf"
}

variable "user_data" {
  default     = "/scripts/install_Ubuntu.sh"
  description = "Path to user data script"
}

#   SECURITY GROUP
variable "aws_security_group_name" {
  default = "ec2_tf"
}

variable "ingress_ports" {
  type        = "list"
  default     = ["8080", "80", "443"]
  description = "List of open ports"
}

variable "ingress_cidr_blocks" {
  type    = "list"
  default = ["0.0.0.0/0"]
}

variable "ssh_ingress_cidr_blocks" {
  type    = "list"
  default = ["0.0.0.0/0"]
}

variable "ssh_from_port" {
  default = "22"
}

variable "ssh_to_port" {
  default = "22"
}

variable "egress_cidr_blocks" {
  type    = "list"
  default = ["0.0.0.0/0"]
}
