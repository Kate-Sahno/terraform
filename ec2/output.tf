output "public_ip" {
  value = "${join(",", aws_instance.masterUbuntu18.*.public_ip)}"
}

output "ec2_ami" {
  value = "${join(",", aws_instance.masterUbuntu18.*.ami)}"
}
