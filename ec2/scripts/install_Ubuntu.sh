#!/bin/bash

# Docker
sudo apt-get install curl apt-transport-https ca-certificates gnupg-agent software-properties-common -y
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-get update -y
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
sudo usermod -aG docker ubuntu

# AWS CLI
sudo apt install unzip -y
sudo curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
sudo unzip awscliv2.zip
sudo ./aws/install
rm -drf ./aws awscliv2.zip

# Add jenkins pkg
sudo apt-get install openjdk-8-jdk -y
sudo wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo echo "deb https://pkg.jenkins.io/debian-stable binary/" >> /etc/apt/sources.list
sudo apt-get update -y
sudo apt-get install jenkins -y
sudo service jenkins start

# Terraform
sudo apt-get install wget -y
sudo wget https://releases.hashicorp.com/terraform/0.12.25/terraform_0.12.25_linux_amd64.zip
sudo unzip terraform_0.12.25_linux_amd64.zip
sudo mv terraform /usr/local/bin/
sudo rm -fd terraform_0.12.25_linux_amd64.zip

sudo rm -rf /var/cache/apt/*
