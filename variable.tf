variable "region" {
  default     = "sa-east-1"
  description = "Default Amazon region"
}

variable "profile" {
  default = "default"
}

variable "aws-key-name" {
  default     = "aws-key-name"
  description = "Default Amazon aws_key_pair"
}
