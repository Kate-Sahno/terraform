resource "aws_db_instance" "db_instance_tf" {
  allocated_storage         = 20
  storage_type              = "gp2"
  engine                    = "mysql"
  engine_version            = "5.7"
  instance_class            = "db.t2.micro"
  port                      = var.port
  name                      = var.db_instance_name
  username                  = var.username
  password                  = var.password
  skip_final_snapshot       = false
  final_snapshot_identifier = false
  # security_group_names      = ["${aws_security_group.db_security_groups_tf.name}"]
}

# resource "aws_security_group" "db_security_groups_tf" {
name = var.security_group_name
ingress {
  from_port   = var.db_instance_port
  to_port     = var.db_instance_port
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}
# vpc_id = "vpc-f0d9d397"
# }
