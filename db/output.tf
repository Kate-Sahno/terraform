output "port" {
  value = "${aws_db_instance.db_instance_tf.port}"
}

output "endpoint" {
  value = "${aws_db_instance.db_instance_tf.address}"
}

output "database_name" {
  value = "${aws_db_instance.db_instance_tf.name}"
}
