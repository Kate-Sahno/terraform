variable "username" {
  default     = "root"
  description = "Default superuser in DB"
}

variable "password" {
  default     = "password"
  description = "Password for superuser"
}

variable "db_instance_name" {
  default     = "test"
  description = "Name for creating database"
}

# variable "security_group_name" {
#   default = "terraform_db"
# }

variable "port" {
  default = 3306
}
